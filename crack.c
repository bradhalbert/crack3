#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"
#include <stdbool.h>

const int PASS_LEN = 50;        // Maximum any password can be
const int HASH_LEN = 33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
/*int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *guess_hash = md5(guess, strlen(guess));

    // Compare the two hashes
    if(strcmp(hash, guess_hash) == 0)
    {
      //free(pass); //free memory
      return 1;
    }
    else
    {
      //free(pass); //free memory
      return 0;
    }

}*/

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{

    int size = 50;
    char **hashes = (char **)malloc(size * sizeof(char *));

    char str[40];
    int i = 0;
    FILE *f = fopen(filename, "r");
    while(fscanf(f, "%s", str) != EOF)
    {
      if(i == size)
      {
        size += 10;
        char **newarr = (char **)realloc(hashes, size * sizeof(char *));
        if(newarr != NULL) hashes = newarr;
        else
        {
          printf("Realloc failed\n");
          exit(1);
        }
      }
      char *newstr = (char *)malloc(strlen(str) + 1 * sizeof(char));
      strcpy(newstr, str);
      hashes[i] = newstr;

      i++;
    }
    fclose(f);
    return hashes;
}


// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{

  struct node *hash = NULL;

  //open the password file
  FILE *f = fopen(filename, "r");

  if(!f)
  {
    printf("Can't open file\n");
    exit(1);
  }

  char pw[50];
  while(fscanf(f, "%s", pw) != EOF)
  {
    //hash the passwords
    char *ph = md5(pw, strlen(pw));

    //put both the hashed and plain text into the entry
    insert (ph, pw, &hash);
    //free the memory
    free(ph);

  }
  fclose(f); // close the file
  return hash; //return the hash node

}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes("hashes.txt");

    // TODO: Read the dictionary file into a binary tree
    node *dict = read_dict("rockyou.txt");

    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)

    for(int i = 8; hashes[i] != NULL; i++)
    {
         node *s; //new hash to store the return value from the search function

         s = search(hashes[i], dict);
         if(s == NULL)
         {
           printf("im sorry the hash %s could not be found\n", hashes[i]);
         }
         else{
           printf("Hashed Password:    %s,    String Password:   %s\n", s->hash, s->pass);
         }

    }

}
