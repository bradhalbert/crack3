// TODO: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
	char pass[50];
	char hash[33];
	struct node *left;
	struct node *right;
} node;

void insert(char *hkey, char *pass, node **leaf);

void print(node *leaf);

node *search(char *hkey, node *leaf);
