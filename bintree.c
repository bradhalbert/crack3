#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bintree.h"


void insert(char *hkey, char *pass, node **leaf)
{
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        strcpy((*leaf)->hash, hkey);
        strcpy((*leaf)->pass, pass);
        /* initialize the children to null */
        (*leaf)->left = NULL;
        (*leaf)->right = NULL;
    }
    else if (strcmp(hkey, (*leaf)->hash) < 0)
        insert (hkey, pass, &((*leaf)->left) );
    else if (strcmp(hkey, (*leaf)->hash) > 0)
        insert (hkey, pass, &((*leaf)->right) );
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s\n", leaf->hash);
	if (leaf->right != NULL) print(leaf->right);
}

// TODO: Modify so the key is the hash to search for
node *search(char *hkey, node *leaf)
{
  if (leaf != NULL)
  {
      if (strcmp(hkey, leaf->hash) == 0)
         return leaf;
      else if (strcmp(hkey, leaf->hash) < 0)
         return search(hkey, leaf->left);
      else
         return search(hkey, leaf->right);
  }
  else return NULL;
}

/*
int main()
{
    node *tree = NULL;

    insert(5, &tree);
    insert(2, &tree);
    insert(8, &tree);
    insert(10, &tree);
    insert(1, &tree);
    insert(4, &tree);
    insert(8, &tree);
    insert(7, &tree);

    print(tree);
}
*/
